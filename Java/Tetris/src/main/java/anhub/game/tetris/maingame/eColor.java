package anhub.game.tetris.maingame;

public enum eColor {
    BLACK, RED, GREEN, BLUE, AQUA, YELLOW, ORANGE, PURPLE;

    public static String getStringColor(eColor color){
        switch (color) {
            case BLACK:
                return "black";
            case RED:
                return "red";
            case GREEN:
                return "green";
            case BLUE:
                return "blue";
            case AQUA:
                return "aqua";
            case YELLOW:
                return "yellow";
            case ORANGE:
                return "orange";
            case PURPLE:
                return "purple";
        }
        return "";
    }
}