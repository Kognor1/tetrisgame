package anhub.game.tetris.maingame;

public class Main {
    private boolean endOfGame;
    private GameField gameField;
    private  ShiftDirection shiftDirection;
    private  boolean isRotateRequested;

    public Main(){
         initFields();
    }
    private  void initFields() {
        endOfGame = false;
        shiftDirection = ShiftDirection.AWAITING;
        isRotateRequested = false;
        gameField = new GameField();
    }
    public void input(ShiftDirection sd, boolean wasRot, boolean wasEsc){

        shiftDirection = sd;

        isRotateRequested = wasRot;

        endOfGame = endOfGame || wasEsc;
    }
    public  int logic(){
        if(shiftDirection != ShiftDirection.AWAITING){ // Если есть запрос на сдвиг фигуры

            /* Пробуем сдвинуть */
            gameField.tryShiftFigure(shiftDirection);
        }

        if(isRotateRequested){ // Если есть запрос на поворот фигуры

            /* Пробуем повернуть */
            gameField.tryRotateFigure();

            /* Ожидаем нового запроса */
            isRotateRequested = false;
        }
        Boolean[] wasShiftLineDown= new Boolean[1];
        wasShiftLineDown[0]=false;
        gameField.letFallDown(wasShiftLineDown);



        /* Если поле переполнено, игра закончена */
        endOfGame = endOfGame || gameField.isOverfilled();
        if(endOfGame)
            return -1;
        else  if (wasShiftLineDown[0]){
            return 1;

        }
        else {
            return 0;
        }
    }

    public boolean isEndOfGame(){
        return endOfGame;
    }

    public Figure getFigure(){
        return gameField.getFigure();
    }

    public eColor getColor(int x, int y){
        return gameField.getColor(x, y);
    }
}
