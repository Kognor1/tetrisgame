package anhub.game.tetris.server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;

public class OracleJDBCEx {
    private static OracleJDBCEx instance = new OracleJDBCEx();
    private static final Logger logger = LogManager.getLogger();

    private Connection connection;

    public OracleJDBCEx(){
        connectToDataBase();
    }

    private void connectToDataBase(){
        Properties props = new Properties();
        props.setProperty("user", "root");
        props.setProperty("password", "mysql_pass");

        TimeZone timeZone = TimeZone.getTimeZone("GMT+7");
        TimeZone.setDefault(timeZone);
        Locale.setDefault(Locale.ENGLISH);

        try {
            connection = DriverManager.getConnection("jdbc:mysql://mysql:3306/tetris", props);
        } catch (SQLException e) {
            logger.error(e);
        }

        checkTablesUsers();
        checkTablesScores();
    }

    private void checkTablesScores() {
        try (PreparedStatement preStatement = connection.prepareStatement("" +
                "CREATE TABLE IF NOT EXISTS users (  " +
                "id INT(10) NOT NULL AUTO_INCREMENT,  " +
                "login TINYTEXT NULL DEFAULT NULL, " +
                " password TINYTEXT NULL DEFAULT NULL, " +
                " PRIMARY KEY (id) USING BTREE);")
        ) {
            preStatement.execute();
        }catch (SQLException e){
            logger.error(e);
        }
    }

    private void checkTablesUsers() {
        try (PreparedStatement preStatement = connection.prepareStatement("" +
                "CREATE TABLE IF NOT EXISTS scores (  " +
                "idScores INT(10) NOT NULL AUTO_INCREMENT, " +
                " idUser INT(10) NOT NULL DEFAULT '0', " +
                " scores INT(10) NULL DEFAULT '0',  " +
                "PRIMARY KEY (idScores) USING BTREE)")
        ){
            preStatement.execute();
        }catch (SQLException e){
            logger.error(e);
        }
    }

    public static OracleJDBCEx getInstance() {
        return instance;
    }

    public void sqlUpdate(String sql){
        logger.info("UPDATE: " + sql);
        try (PreparedStatement preStatement = connection.prepareStatement(sql)){
            preStatement.executeUpdate();
        }
        catch (SQLException e){
            logger.error(e);
        }
    }

    public String[] getSqlQuery(String sql) {
        logger.info("QUERY: " + sql);
        ResultSet result = null;
        String[] res = new String [2] ;
        boolean empty=true;
        try (PreparedStatement preStatement = connection.prepareStatement(sql)){

            result = preStatement.executeQuery();

            while ( result.next()){
                empty=false;
                res[0]=result.getString(1);
                res[1]=result.getString(2);
            }
        }
        catch (SQLException e){
            logger.error(e);
        }
        finally {
            assert result != null;
            try {
                result.close();
            } catch (SQLException e) {
                logger.error(e);
            }
        }

        if(empty){
            res = new String[0];
        }

        return res;
    }
}
