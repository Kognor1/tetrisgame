package AnHub.Game.tetris.controllersTests;
import anhub.game.tetris.controller.Authorization;
import anhub.game.tetris.server.OracleJDBCEx;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

public class AuthorizationTest {
	private Authorization authorization;
	private OracleJDBCEx bdConnect= OracleJDBCEx.getInstance();

	@Before
	public void setUp() {
		authorization = new Authorization();
	}

	@Test
	public void authorizationNewPlayerTest() {
		String message = "newPlayerControllersTest+111=";
		Map<String,String> dictionary = authorization.list(message);
		String site = dictionary.get("site");
		assertEquals("index.html", site);
		bdConnect.sqlUpdate("DELETE from users where login='newPlayerControllersTest'");
	}

	@Test
	public void authorizationFailTest() {
		String message = "newPlayerControllersTest+111=";
		authorization.list(message);
		message = "newPlayerControllersTest+222=";
		Map<String,String> dictionary = authorization.list(message);
		String site = dictionary.get("site");
		assertEquals("Error.html", site);
		bdConnect.sqlUpdate("DELETE from users where login='newPlayerControllersTest'");
	}

	@Test
	public void authorizationSuccessTest() {
		String message = "newPlayerControllersTest+111=";
		authorization.list(message);
		Map<String,String> dictionary = authorization.list(message);
		String site = dictionary.get("site");
		assertEquals("index.html", site);
		bdConnect.sqlUpdate("DELETE from users where login='newPlayerControllersTest'");
	}
}