package AnHub.Game.tetris.maingameTests;

import anhub.game.tetris.maingame.*;
import org.junit.Test;

import static org.junit.Assert.*;

public class CoordMaskTest {

    //I_FORM
    @Test
    public void IFormNormalRotationTest(){
        Coord coord = new Coord(Constants.COUNT_CELLS_X/2, Constants.COUNT_CELLS_Y + Constants.OFFSET_TOP - 1);
        Figure figure = new Figure(coord, RotationMode.NORMAL, FigureForm.I_FORM);

        Coord metaPointCoord = figure.getMetaPointCoords();
        int x = metaPointCoord.getX();
        int y = metaPointCoord.getY();
        Coord[] coords = figure.getCoords();

        assertEquals(coords[0].getX(), x);
        assertEquals(coords[1].getX(), x);
        assertEquals(coords[2].getX(), x);
        assertEquals(coords[3].getX(), x);

        assertEquals(coords[0].getY(), y);
        assertEquals(coords[1].getY(), y - 1);
        assertEquals(coords[2].getY(), y - 2);
        assertEquals(coords[3].getY(), y - 3);
    }

    @Test
    public void IFormInvertRotationTest(){
        Coord coord = new Coord(Constants.COUNT_CELLS_X/2, Constants.COUNT_CELLS_Y + Constants.OFFSET_TOP - 1);
        Figure figure = new Figure(coord, RotationMode.INVERT, FigureForm.I_FORM);

        Coord metaPointCoord = figure.getMetaPointCoords();
        int x = metaPointCoord.getX();
        int y = metaPointCoord.getY();
        Coord[] coords = figure.getCoords();

        assertEquals(coords[0].getX(), x);
        assertEquals(coords[1].getX(), x);
        assertEquals(coords[2].getX(), x);
        assertEquals(coords[3].getX(), x);

        assertEquals(coords[0].getY(), y);
        assertEquals(coords[1].getY(), y - 1);
        assertEquals(coords[2].getY(), y - 2);
        assertEquals(coords[3].getY(), y - 3);
    }

    @Test
    public void IFormFlipOnesRotationTest(){
        Coord coord = new Coord(Constants.COUNT_CELLS_X/2, Constants.COUNT_CELLS_Y + Constants.OFFSET_TOP - 1);
        Figure figure = new Figure(coord, RotationMode.FLIP_ONES, FigureForm.I_FORM);

        Coord metaPointCoord = figure.getMetaPointCoords();
        int x = metaPointCoord.getX();
        int y = metaPointCoord.getY();
        Coord[] coords = figure.getCoords();

        assertEquals(coords[0].getX(), x);
        assertEquals(coords[1].getX(), x + 1);
        assertEquals(coords[2].getX(), x + 2);
        assertEquals(coords[3].getX(), x + 3);

        assertEquals(coords[0].getY(), y);
        assertEquals(coords[1].getY(), y);
        assertEquals(coords[2].getY(), y);
        assertEquals(coords[3].getY(), y);
    }

    @Test
    public void IFormFlipTwiceRotationTest(){
        Coord coord = new Coord(Constants.COUNT_CELLS_X/2, Constants.COUNT_CELLS_Y + Constants.OFFSET_TOP - 1);
        Figure figure = new Figure(coord, RotationMode.FLIP_TWICE, FigureForm.I_FORM);

        Coord metaPointCoord = figure.getMetaPointCoords();
        int x = metaPointCoord.getX();
        int y = metaPointCoord.getY();
        Coord[] coords = figure.getCoords();

        assertEquals(coords[0].getX(), x);
        assertEquals(coords[1].getX(), x + 1);
        assertEquals(coords[2].getX(), x + 2);
        assertEquals(coords[3].getX(), x + 3);

        assertEquals(coords[0].getY(), y);
        assertEquals(coords[1].getY(), y);
        assertEquals(coords[2].getY(), y);
        assertEquals(coords[3].getY(), y);
    }

    //J_FORM
    @Test
    public void JFormNormalRotationTest(){
        Coord coord = new Coord(Constants.COUNT_CELLS_X/2, Constants.COUNT_CELLS_Y + Constants.OFFSET_TOP - 1);
        Figure figure = new Figure(coord, RotationMode.NORMAL, FigureForm.J_FORM);

        Coord metaPointCoord = figure.getMetaPointCoords();
        int x = metaPointCoord.getX();
        int y = metaPointCoord.getY();
        Coord[] coords = figure.getCoords();

        assertEquals(coords[0].getX(), x + 1);
        assertEquals(coords[1].getX(), x + 1);
        assertEquals(coords[2].getX(), x + 1);
        assertEquals(coords[3].getX(), x);

        assertEquals(coords[0].getY(), y);
        assertEquals(coords[1].getY(), y - 1);
        assertEquals(coords[2].getY(), y - 2);
        assertEquals(coords[3].getY(), y - 2);
    }

    @Test
    public void JFormInvertRotationTest(){
        Coord coord = new Coord(Constants.COUNT_CELLS_X/2, Constants.COUNT_CELLS_Y + Constants.OFFSET_TOP - 1);
        Figure figure = new Figure(coord, RotationMode.INVERT, FigureForm.J_FORM);

        Coord metaPointCoord = figure.getMetaPointCoords();
        int x = metaPointCoord.getX();
        int y = metaPointCoord.getY();
        Coord[] coords = figure.getCoords();

        assertEquals(coords[0].getX(), x + 1);
        assertEquals(coords[1].getX(), x);
        assertEquals(coords[2].getX(), x);
        assertEquals(coords[3].getX(), x);

        assertEquals(coords[0].getY(), y);
        assertEquals(coords[1].getY(), y);
        assertEquals(coords[2].getY(), y - 1);
        assertEquals(coords[3].getY(), y - 2);
    }

    @Test
    public void JFormFlipOnesRotationTest(){
        Coord coord = new Coord(Constants.COUNT_CELLS_X/2, Constants.COUNT_CELLS_Y + Constants.OFFSET_TOP - 1);
        Figure figure = new Figure(coord, RotationMode.FLIP_ONES, FigureForm.J_FORM);

        Coord metaPointCoord = figure.getMetaPointCoords();
        int x = metaPointCoord.getX();
        int y = metaPointCoord.getY();
        Coord[] coords = figure.getCoords();

        assertEquals(coords[0].getX(), x);
        assertEquals(coords[1].getX(), x);
        assertEquals(coords[2].getX(), x + 1);
        assertEquals(coords[3].getX(), x + 2);

        assertEquals(coords[0].getY(), y);
        assertEquals(coords[1].getY(), y - 1);
        assertEquals(coords[2].getY(), y - 1);
        assertEquals(coords[3].getY(), y - 1);
    }

    @Test
    public void JFormFlipTwiceRotationTest(){
        Coord coord = new Coord(Constants.COUNT_CELLS_X/2, Constants.COUNT_CELLS_Y + Constants.OFFSET_TOP - 1);
        Figure figure = new Figure(coord, RotationMode.FLIP_TWICE, FigureForm.J_FORM);

        Coord metaPointCoord = figure.getMetaPointCoords();
        int x = metaPointCoord.getX();
        int y = metaPointCoord.getY();
        Coord[] coords = figure.getCoords();

        assertEquals(coords[0].getX(), x);
        assertEquals(coords[1].getX(), x + 1);
        assertEquals(coords[2].getX(), x + 2);
        assertEquals(coords[3].getX(), x + 2);

        assertEquals(coords[0].getY(), y);
        assertEquals(coords[1].getY(), y);
        assertEquals(coords[2].getY(), y);
        assertEquals(coords[3].getY(), y - 1);
    }

    //L_FORM
    @Test
    public void LFormNormalRotationTest(){
        Coord coord = new Coord(Constants.COUNT_CELLS_X/2, Constants.COUNT_CELLS_Y + Constants.OFFSET_TOP - 1);
        Figure figure = new Figure(coord, RotationMode.NORMAL, FigureForm.L_FORM);

        Coord metaPointCoord = figure.getMetaPointCoords();
        int x = metaPointCoord.getX();
        int y = metaPointCoord.getY();
        Coord[] coords = figure.getCoords();

        assertEquals(coords[0].getX(), x);
        assertEquals(coords[1].getX(), x);
        assertEquals(coords[2].getX(), x);
        assertEquals(coords[3].getX(), x + 1);

        assertEquals(coords[0].getY(), y);
        assertEquals(coords[1].getY(), y - 1);
        assertEquals(coords[2].getY(), y - 2);
        assertEquals(coords[3].getY(), y - 2);
    }

    @Test
    public void LFormInvertRotationTest(){
        Coord coord = new Coord(Constants.COUNT_CELLS_X/2, Constants.COUNT_CELLS_Y + Constants.OFFSET_TOP - 1);
        Figure figure = new Figure(coord, RotationMode.INVERT, FigureForm.L_FORM);

        Coord metaPointCoord = figure.getMetaPointCoords();
        int x = metaPointCoord.getX();
        int y = metaPointCoord.getY();
        Coord[] coords = figure.getCoords();

        assertEquals(coords[0].getX(), x);
        assertEquals(coords[1].getX(), x + 1);
        assertEquals(coords[2].getX(), x + 1);
        assertEquals(coords[3].getX(), x + 1);

        assertEquals(coords[0].getY(), y);
        assertEquals(coords[1].getY(), y);
        assertEquals(coords[2].getY(), y - 1);
        assertEquals(coords[3].getY(), y - 2);
    }

    @Test
    public void LFormFlipOnesRotationTest(){
        Coord coord = new Coord(Constants.COUNT_CELLS_X/2, Constants.COUNT_CELLS_Y + Constants.OFFSET_TOP - 1);
        Figure figure = new Figure(coord, RotationMode.FLIP_ONES, FigureForm.L_FORM);

        Coord metaPointCoord = figure.getMetaPointCoords();
        int x = metaPointCoord.getX();
        int y = metaPointCoord.getY();
        Coord[] coords = figure.getCoords();

        assertEquals(coords[0].getX(), x);
        assertEquals(coords[1].getX(), x);
        assertEquals(coords[2].getX(), x + 1);
        assertEquals(coords[3].getX(), x + 2);

        assertEquals(coords[0].getY(), y - 1);
        assertEquals(coords[1].getY(), y);
        assertEquals(coords[2].getY(), y);
        assertEquals(coords[3].getY(), y);
    }

    @Test
    public void LFormFlipTwiceRotationTest(){
        Coord coord = new Coord(Constants.COUNT_CELLS_X/2, Constants.COUNT_CELLS_Y + Constants.OFFSET_TOP - 1);
        Figure figure = new Figure(coord, RotationMode.FLIP_TWICE, FigureForm.L_FORM);

        Coord metaPointCoord = figure.getMetaPointCoords();
        int x = metaPointCoord.getX();
        int y = metaPointCoord.getY();
        Coord[] coords = figure.getCoords();

        assertEquals(coords[0].getX(), x);
        assertEquals(coords[1].getX(), x + 1);
        assertEquals(coords[2].getX(), x + 2);
        assertEquals(coords[3].getX(), x + 2);

        assertEquals(coords[0].getY(), y - 1);
        assertEquals(coords[1].getY(), y - 1);
        assertEquals(coords[2].getY(), y - 1);
        assertEquals(coords[3].getY(), y);
    }

    //O_FORM
    @Test
    public void OFormTest(){
        Coord coord = new Coord(Constants.COUNT_CELLS_X/2, Constants.COUNT_CELLS_Y + Constants.OFFSET_TOP - 1);
        Figure figure = new Figure(coord, RotationMode.NORMAL, FigureForm.O_FORM);

        Coord metaPointCoord = figure.getMetaPointCoords();
        int x = metaPointCoord.getX();
        int y = metaPointCoord.getY();
        Coord[] coords = figure.getCoords();

        assertEquals(coords[0].getX(), x);
        assertEquals(coords[1].getX(), x);
        assertEquals(coords[2].getX(), x + 1);
        assertEquals(coords[3].getX(), x + 1);

        assertEquals(coords[0].getY(), y);
        assertEquals(coords[1].getY(), y - 1);
        assertEquals(coords[2].getY(), y - 1);
        assertEquals(coords[3].getY(), y);
    }

    //S_FORM
    @Test
    public void SFormNormalRotationTest(){
        Coord coord = new Coord(Constants.COUNT_CELLS_X/2, Constants.COUNT_CELLS_Y + Constants.OFFSET_TOP - 1);
        Figure figure = new Figure(coord, RotationMode.NORMAL, FigureForm.S_FORM);

        Coord metaPointCoord = figure.getMetaPointCoords();
        int x = metaPointCoord.getX();
        int y = metaPointCoord.getY();
        Coord[] coords = figure.getCoords();

        assertEquals(coords[0].getX(), x);
        assertEquals(coords[1].getX(), x + 1);
        assertEquals(coords[2].getX(), x + 1);
        assertEquals(coords[3].getX(), x + 2);

        assertEquals(coords[0].getY(), y - 1);
        assertEquals(coords[1].getY(), y - 1);
        assertEquals(coords[2].getY(), y);
        assertEquals(coords[3].getY(), y);
    }

    @Test
    public void SFormInvertRotationTest(){
        Coord coord = new Coord(Constants.COUNT_CELLS_X/2, Constants.COUNT_CELLS_Y + Constants.OFFSET_TOP - 1);
        Figure figure = new Figure(coord, RotationMode.INVERT, FigureForm.S_FORM);

        Coord metaPointCoord = figure.getMetaPointCoords();
        int x = metaPointCoord.getX();
        int y = metaPointCoord.getY();
        Coord[] coords = figure.getCoords();

        assertEquals(coords[0].getX(), x);
        assertEquals(coords[1].getX(), x + 1);
        assertEquals(coords[2].getX(), x + 1);
        assertEquals(coords[3].getX(), x + 2);

        assertEquals(coords[0].getY(), y - 1);
        assertEquals(coords[1].getY(), y - 1);
        assertEquals(coords[2].getY(), y);
        assertEquals(coords[3].getY(), y);
    }

    @Test
    public void SFormFlipOnesRotationTest(){
        Coord coord = new Coord(Constants.COUNT_CELLS_X/2, Constants.COUNT_CELLS_Y + Constants.OFFSET_TOP - 1);
        Figure figure = new Figure(coord, RotationMode.FLIP_ONES, FigureForm.S_FORM);

        Coord metaPointCoord = figure.getMetaPointCoords();
        int x = metaPointCoord.getX();
        int y = metaPointCoord.getY();
        Coord[] coords = figure.getCoords();

        assertEquals(coords[0].getX(), x);
        assertEquals(coords[1].getX(), x);
        assertEquals(coords[2].getX(), x + 1);
        assertEquals(coords[3].getX(), x + 1);

        assertEquals(coords[0].getY(), y);
        assertEquals(coords[1].getY(), y - 1);
        assertEquals(coords[2].getY(), y - 1);
        assertEquals(coords[3].getY(), y - 2);
    }

    @Test
    public void SFormFlipTwiceRotationTest(){
        Coord coord = new Coord(Constants.COUNT_CELLS_X/2, Constants.COUNT_CELLS_Y + Constants.OFFSET_TOP - 1);
        Figure figure = new Figure(coord, RotationMode.FLIP_TWICE, FigureForm.S_FORM);

        Coord metaPointCoord = figure.getMetaPointCoords();
        int x = metaPointCoord.getX();
        int y = metaPointCoord.getY();
        Coord[] coords = figure.getCoords();

        assertEquals(coords[0].getX(), x);
        assertEquals(coords[1].getX(), x);
        assertEquals(coords[2].getX(), x + 1);
        assertEquals(coords[3].getX(), x + 1);

        assertEquals(coords[0].getY(), y);
        assertEquals(coords[1].getY(), y - 1);
        assertEquals(coords[2].getY(), y - 1);
        assertEquals(coords[3].getY(), y - 2);
    }

    //Z_FORM
    @Test
    public void ZFormNormalRotationTest(){
        Coord coord = new Coord(Constants.COUNT_CELLS_X/2, Constants.COUNT_CELLS_Y + Constants.OFFSET_TOP - 1);
        Figure figure = new Figure(coord, RotationMode.NORMAL, FigureForm.Z_FORM);

        Coord metaPointCoord = figure.getMetaPointCoords();
        int x = metaPointCoord.getX();
        int y = metaPointCoord.getY();
        Coord[] coords = figure.getCoords();

        assertEquals(coords[0].getX(), x);
        assertEquals(coords[1].getX(), x + 1);
        assertEquals(coords[2].getX(), x + 1);
        assertEquals(coords[3].getX(), x + 2);

        assertEquals(coords[0].getY(), y);
        assertEquals(coords[1].getY(), y);
        assertEquals(coords[2].getY(), y - 1);
        assertEquals(coords[3].getY(), y - 1);
    }

    @Test
    public void ZFormInvertRotationTest(){
        Coord coord = new Coord(Constants.COUNT_CELLS_X/2, Constants.COUNT_CELLS_Y + Constants.OFFSET_TOP - 1);
        Figure figure = new Figure(coord, RotationMode.INVERT, FigureForm.Z_FORM);

        Coord metaPointCoord = figure.getMetaPointCoords();
        int x = metaPointCoord.getX();
        int y = metaPointCoord.getY();
        Coord[] coords = figure.getCoords();

        assertEquals(coords[0].getX(), x);
        assertEquals(coords[1].getX(), x + 1);
        assertEquals(coords[2].getX(), x + 1);
        assertEquals(coords[3].getX(), x + 2);

        assertEquals(coords[0].getY(), y);
        assertEquals(coords[1].getY(), y);
        assertEquals(coords[2].getY(), y - 1);
        assertEquals(coords[3].getY(), y - 1);
    }

    @Test
    public void ZFormFlipOnesRotationTest(){
        Coord coord = new Coord(Constants.COUNT_CELLS_X/2, Constants.COUNT_CELLS_Y + Constants.OFFSET_TOP - 1);
        Figure figure = new Figure(coord, RotationMode.FLIP_ONES, FigureForm.Z_FORM);

        Coord metaPointCoord = figure.getMetaPointCoords();
        int x = metaPointCoord.getX();
        int y = metaPointCoord.getY();
        Coord[] coords = figure.getCoords();

        assertEquals(coords[0].getX(), x + 1);
        assertEquals(coords[1].getX(), x + 1);
        assertEquals(coords[2].getX(), x);
        assertEquals(coords[3].getX(), x);

        assertEquals(coords[0].getY(), y);
        assertEquals(coords[1].getY(), y - 1);
        assertEquals(coords[2].getY(), y - 1);
        assertEquals(coords[3].getY(), y - 2);
    }

    @Test
    public void ZFormFlipTwiceRotationTest(){
        Coord coord = new Coord(Constants.COUNT_CELLS_X/2, Constants.COUNT_CELLS_Y + Constants.OFFSET_TOP - 1);
        Figure figure = new Figure(coord, RotationMode.FLIP_TWICE, FigureForm.Z_FORM);

        Coord metaPointCoord = figure.getMetaPointCoords();
        int x = metaPointCoord.getX();
        int y = metaPointCoord.getY();
        Coord[] coords = figure.getCoords();

        assertEquals(coords[0].getX(), x + 1);
        assertEquals(coords[1].getX(), x + 1);
        assertEquals(coords[2].getX(), x);
        assertEquals(coords[3].getX(), x);

        assertEquals(coords[0].getY(), y);
        assertEquals(coords[1].getY(), y - 1);
        assertEquals(coords[2].getY(), y - 1);
        assertEquals(coords[3].getY(), y - 2);
    }

    //T_FORM
    @Test
    public void TFormNormalRotationTest(){
        Coord coord = new Coord(Constants.COUNT_CELLS_X/2, Constants.COUNT_CELLS_Y + Constants.OFFSET_TOP - 1);
        Figure figure = new Figure(coord, RotationMode.NORMAL, FigureForm.T_FORM);

        Coord metaPointCoord = figure.getMetaPointCoords();
        int x = metaPointCoord.getX();
        int y = metaPointCoord.getY();
        Coord[] coords = figure.getCoords();

        assertEquals(coords[0].getX(), x);
        assertEquals(coords[1].getX(), x + 1);
        assertEquals(coords[2].getX(), x + 1);
        assertEquals(coords[3].getX(), x + 2);

        assertEquals(coords[0].getY(), y);
        assertEquals(coords[1].getY(), y);
        assertEquals(coords[2].getY(), y - 1);
        assertEquals(coords[3].getY(), y);
    }

    @Test
    public void TFormInvertRotationTest(){
        Coord coord = new Coord(Constants.COUNT_CELLS_X/2, Constants.COUNT_CELLS_Y + Constants.OFFSET_TOP - 1);
        Figure figure = new Figure(coord, RotationMode.INVERT, FigureForm.T_FORM);

        Coord metaPointCoord = figure.getMetaPointCoords();
        int x = metaPointCoord.getX();
        int y = metaPointCoord.getY();
        Coord[] coords = figure.getCoords();

        assertEquals(coords[0].getX(), x);
        assertEquals(coords[1].getX(), x + 1);
        assertEquals(coords[2].getX(), x + 1);
        assertEquals(coords[3].getX(), x + 2);

        assertEquals(coords[0].getY(), y - 1);
        assertEquals(coords[1].getY(), y - 1);
        assertEquals(coords[2].getY(), y);
        assertEquals(coords[3].getY(), y - 1);
    }

    @Test
    public void TFormFlipOnesRotationTest(){
        Coord coord = new Coord(Constants.COUNT_CELLS_X/2, Constants.COUNT_CELLS_Y + Constants.OFFSET_TOP - 1);
        Figure figure = new Figure(coord, RotationMode.FLIP_ONES, FigureForm.T_FORM);

        Coord metaPointCoord = figure.getMetaPointCoords();
        int x = metaPointCoord.getX();
        int y = metaPointCoord.getY();
        Coord[] coords = figure.getCoords();

        assertEquals(coords[0].getX(), x + 1);
        assertEquals(coords[1].getX(), x + 1);
        assertEquals(coords[2].getX(), x);
        assertEquals(coords[3].getX(), x + 1);

        assertEquals(coords[0].getY(), y);
        assertEquals(coords[1].getY(), y - 1);
        assertEquals(coords[2].getY(), y - 1);
        assertEquals(coords[3].getY(), y - 2);
    }

    @Test
    public void TFormFlipTwiceRotationTest(){
        Coord coord = new Coord(Constants.COUNT_CELLS_X/2, Constants.COUNT_CELLS_Y + Constants.OFFSET_TOP - 1);
        Figure figure = new Figure(coord, RotationMode.FLIP_TWICE, FigureForm.T_FORM);

        Coord metaPointCoord = figure.getMetaPointCoords();
        int x = metaPointCoord.getX();
        int y = metaPointCoord.getY();
        Coord[] coords = figure.getCoords();

        assertEquals(coords[0].getX(), x);
        assertEquals(coords[1].getX(), x);
        assertEquals(coords[2].getX(), x + 1);
        assertEquals(coords[3].getX(), x);

        assertEquals(coords[0].getY(), y);
        assertEquals(coords[1].getY(), y - 1);
        assertEquals(coords[2].getY(), y - 1);
        assertEquals(coords[3].getY(), y - 2);
    }
}
